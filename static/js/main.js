// Utilities
function containsClass(element, className)
{
  return ("" + element.className + "").indexOf("" + className + "") > -1;
};

function toggleClass(element, className)
{
  return (containsClass(element, className) ? removeClass : addClass)(element, className);
};

function addClass(element, className)
{
  return containsClass(element, className) ? false : (element.className += " " + className);
};

function removeClass(element, className)
{
  return element.className = ("" + element.className + "").replace("" + className + "", " ");
};

// General
var doc = document.documentElement,
    body = document.body,
    mainContent = document.getElementById("content"),
    desktop = 992;

removeClass(doc, "no-js")
addClass(doc, "js")

// Mobile Navigation

var menuBar = document.querySelector(".navbar"),
    logo = menuBar.querySelector(".site-title"),
    menuContainer = menuBar.querySelector(".container"),
    menuNav = menuContainer.querySelector("nav"),
    menuToggle = `<svg xmlns="http://www.w3.org/2000/svg"
                       width="37.5"
                       height="25"
                       viewBox="0 0 37.5 25">
                    <g id="Group_226"
                       transform="translate(-1043.5 -30.5)">
                      <path id="burger_icon"
                            d="M3,31H40.5V26.833H3V31ZM3,20.583H40.5V16.417H3v4.167ZM3,6v4.167H40.5V6Z"
                            transform="translate(1040.5 24.5)"/>
                    </g>
                  </svg>`,
    menuToggleWrapper = document.createElement("span"),
    contentOverlay = "<div class='js-overlay'></div>",
    headerHeight = logo.offsetHeight;

createDomMobileNavigation();

toggleMobileNavigation();

if(containsClass(body, "page-index")) scrollMobileNavigation();

function createDomMobileNavigation()
{
  // create the necessary DOM
  addClass(body, "js-navbar-enabled");
  // add toogle button
  addClass(menuToggleWrapper, "js-navbar-toggle")

  menuToggleWrapper.innerHTML = menuToggle;

  menuContainer.prepend(menuToggleWrapper);
  // clone the Navigation
  mobileNavigation = menuContainer.cloneNode(true);

  addClass(mobileNavigation, "js-navbar")

  body.append(mobileNavigation);
  // add mobile overlay
  mainContent.innerHTML += contentOverlay;
}

function toggleMobileNavigation()
{
  menuToggleWrapper.addEventListener("click", function(ev)
  {
    toggleClass(body, "open");
  });
  document.querySelector("#content .js-overlay").addEventListener("click", function(ev)
  {
    removeClass(body, "open");
  });
}

function scrollMobileNavigation()
{
  // On scroll make background color
  window.addEventListener("scroll", function()
  {
    if(window.pageYOffset > headerHeight)
    {
      addClass(body, "js-navbar-opaque");
    }
    else
    {
      removeClass(body, "js-navbar-opaque");
    }
  });
}
