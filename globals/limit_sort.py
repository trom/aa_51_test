from datetime import datetime

def limit_sort(collection, max_items=None):
    for item in collection:
        item['published_date'] = item['published_date'].strip()
    collection.sort(
        key=lambda x: datetime.strptime(x['published_date'], '%Y-%m-%d')
    )
    if max_items is None:
        return collection
    return collection[-max_items:]
